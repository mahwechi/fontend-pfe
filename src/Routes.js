import React,  { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Products from './pages/Products';
import Recettes from './pages/recettes';
import Contact from './pages/Contact';
// FREE
import bienfaits from './pages/bienfaits';
import HomePage from './pages/HomePage';
import login from './pages/login';
import register from './pages/register';
import Product from "./pages/Product";
import Recette from "./pages/Recette";
import banner from "./pages/banner";
import profil from "./pages/profil";
import facebook from "./pages/facebook";

import Cart from "./pages/Cart";


class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/recettes' component={Recettes} />
        <Route exact path='/Products' component={Products} />
        <Route exact path='/bienfaits' component={bienfaits} />
        <Route exact path='/contact' component={Contact} />
        <Route exact path='/login' component={login} />
        <Route exact path='/register' component={register} />
        <Route exact path='/banner' component={banner} />
        <Route exact path='/profil' component={profil} />
        
        <Route exact path="/products/:id" component={Product} />
      <Route path="/cart" component={Cart} />
      <Route exact path="/recettes/:id" component={Recette} />
      <Route exact path="/facebook" component={facebook} />
    
        {/* <Route exact path='/:product_id' component={details} /> */}
     
       
        >
            
        <Route
          render={function() {
            return <h1>Not Found</h1>;
          }} 
        />
      </Switch>
    );
  }
}

export default Routes;
