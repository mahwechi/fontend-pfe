
import React, { Component } from 'react'
import { MDBContainer, MDBIcon, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBRow, MDBCol  } from "mdbreact";
import './index.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { MDBAnimation } from "mdbreact";


export default class recettes extends Component {
  state = {
    recettes: [],
    
  };
  componentDidMount(){
    
      axios.get(`http://127.0.0.1:8000/api/recettes.json`)
      .then(res => {
        const recettes = res.data;
        this.setState({ recettes });
      })  };

  render() {
    return (
     <div className="bg">
   <MDBContainer>
   <MDBAnimation reveal type="fadeInRight">
   <h2 className="h1-responsive text-center text-black"> <MDBIcon icon="utensils" /> Nos Meilleurs  Recettes</h2>
   </MDBAnimation>
   <p className="text-center w-responsive mx-auto text-black">
   Pour vous donner des idées, voici une sélection de recettes qui
    feront passer un excellent moment à vos convives. 
    Vous trouverez le plat parfait ave notre huile d'olive.
          </p>
      <MDBRow>
  
      { this.state.recettes.map(recette =>
        <MDBCol key={recette.id}  size="4" className='md-0 mb-4'>
         
          <MDBCard >
            <MDBCardImage hover zoom className="img-fluid" src={ 'http://127.0.0.1:8000/uploads/images/'+ recette.image } />
            <MDBCardBody >
              <MDBCardTitle className="text-lowercase font-weight-bold color-light-green">
              <MDBIcon icon="utensils" /> {recette.nom}</MDBCardTitle>
              <MDBCardText> {recette.ingredients} </MDBCardText>
            
            </MDBCardBody>
            <Link  to={'/recettes/' + recette.id}  className="btn  btn-light-green">
            
            Savoir Plus </Link>
          
            <br></br>
          </MDBCard>
        
        </MDBCol>
         )} 
      </MDBRow>
     

      </MDBContainer>
      </div>
    )
  }
}

