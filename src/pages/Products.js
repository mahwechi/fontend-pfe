import React, { Component } from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import {
  MDBIcon
} from "mdbreact";
import './index.css';
import axios from 'axios';
import { MDBAnimation } from "mdbreact";
import ProductItem from "../components/ProductItem";
import ProductsApi from "../api/products";

class PillsPage extends Component {
  state = {
    products: [],
    search: "",
    categories: [],
};

componentDidMount(){
 
  axios.get(`http://127.0.0.1:8000/api/products.json`)
      .then(res => {
        const products = res.data;
  
        this.setState({ products });

       }) 
}
  // componentDidMount(){
    
  //     axios.get(`http://127.0.0.1:8000/api/products.json`)
  //     .then(res => {
  //       const products = res.data;
  //       this.setState({ products });
  //<ProductItem product={product}
  //     })  };


      onchange = e => {
        this.setState({ search: e.target.value });
      };
    
  render() {
    const { search } = this.state;
    
    const filteredProducts = this.state.products.filter(product => {
      return product.name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
    });
    return (
      <>
      <MDBContainer>

        <MDBRow> 
        <MDBCol md="12" >
        <MDBAnimation reveal type="fadeInLeft">
          <h2 className="h1-responsive text-center mx-auto">
          Liste des produits disponibles 
          </h2></MDBAnimation>
          <p className="text-center w-responsive mx-auto">
          Cette liste des produits Rivière d'or comprend des produits 
          tels que la collection premium , l'extra virgin olive oil , 
          la collection biologique et les saveurs.
          </p>
          </MDBCol>
  <MDBCol md="6">
      <div className="input-group md-form form-sm form-1 pl-0">
        <div className="input-group-prepend">
          <span className="input-group-text purple lighten-3" id="basic-text1">
            <MDBIcon className="text-white" icon="search" />
          </span>
        </div>
        <input className="form-control my-0 py-1" type="text" onChange={this.onchange} placeholder="Search" aria-label="Search" />
      </div>  </MDBCol>
       <MDBCol md="3">
      <select className="browser-default custom-select md-form form-sm form-1 pl-0">
          <option >  Catégories </option>
          <option value="1"> Vierge Extra  </option>
          <option value="2"> Pack cadeaux</option>
          <option value="3"> Les saveurs </option>
          <option value="3"> Collection Bio </option>
        </select>
  
        </MDBCol>
        <MDBCol md="3">
      <select className="browser-default custom-select md-form form-sm form-1 pl-0">
          <option >   Prix  </option>
          <option value="1"> de moin au plus cher  </option>
          <option value="2">  de plus au moin cher </option>

        </select>
  
        </MDBCol>
          {  filteredProducts.map(product =>
          
          <MDBCol key={product.id} ize='4'  style={{ padding:"10px"}} >

          <ProductItem product={product}></ProductItem>
                      
          </MDBCol>  )}
        </MDBRow>
      </MDBContainer>   
      </>
    );
  }
}

export default PillsPage;