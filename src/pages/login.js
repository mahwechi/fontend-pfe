import React, { Component} from "react";
import { MDBContainer,MDBCard,MDBIcon, MDBRow, MDBCol,MDBLink,MDBInput,MDBBtn } from 'mdbreact';
import axios from 'axios';

class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",

    };
  }
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  submitHandler = e => {
    e.preventDefault();
    console.log(this.state);
    var bodyFormData = new FormData();
    bodyFormData.set('username', this.state.username);
    bodyFormData.set('password', this.state.password);

    axios({
      method: 'post',
      url: 'http://127.0.0.1:8000/api/users/',
      data: bodyFormData,
      headers: {'Content-Type': 'multipart/form-data' }
      })
      .then(function (response) {
          //handle success
          //window.location = "/profil";
          alert("Hello! I am an alert box!");
          console.log(response.data);
      })
      .catch(function (response) {
          //handle error
          console.log(response);
      });
  };
  render(){
    const { username, password } = this.state;
return (
<MDBContainer>
  <MDBRow>
     <div className="alert alert-success hide">Form submitted successfully</div>
  <MDBCol md="12" className="mb-4" className="connex-card">
                        <MDBCard id="coverConnex" className="card-image" >
                            <div className="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4 rounded">
                                <div>
                                    <h6 className="white-text">
                                        <MDBIcon size="2x" icon="user" />             </h6>
                                    <h3 className="py-3 font-weight-bold">
                                        <strong>Connexion</strong>   
                                    </h3>
                                    <p className="pb-3">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                        Repellat fugiat, laboriosam, voluptatem, optio vero odio
                                        nam sit officia accusamus minus error nisi architecto
                                        nulla ipsum dignissimos. Odit sed qui, dolorum!
              </p>
                                </div>
                            </div>
                        </MDBCard>
                    </MDBCol>
    <MDBCol md="12">

    <div style={{ padding: 20 }}>
        <h3>Sign in to your account</h3>
        <hr />


        <form   onSubmit={this.submitHandler}>
        <div className="grey-text">
          <MDBInput label="Type your username" icon="envelope" group type="text"
            success="right" 
            value={this.username}
            onChange={this.changeHandler}
            name="username"
                   />
          <MDBInput label="Type your password" className="form-control" icon="lock" group type="password"
             value={this.password}
           onChange={this.changeHandler}
           name="password"
           />
        </div>

        <div className="text-center">
          <MDBBtn type="submit" >Login</MDBBtn>
          <MDBLink to='/PageInscri'>Do not have an account? Sign Up Now</MDBLink>

        </div>
        <div className="alert alert-success hide">Form submitted successfully</div>
      </form>
             
            </div>
      
                                 
    </MDBCol>
  </MDBRow>
</MDBContainer>
);
}
}



export default login;