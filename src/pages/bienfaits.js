
import React, { Component } from 'react'
import { MDBContainer, MDBIcon, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBRow, MDBCol  } from "mdbreact";
import './index.css';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { MDBAnimation } from "mdbreact";



export default class bienfaits extends Component {
  state = {
    astuces: [],
    
  };
  componentDidMount(){
    
      axios.get(`http://127.0.0.1:8000/api/astuces.json`)
      .then(res => {
        const astuces = res.data;
        this.setState({ astuces });
      })  };

  render() {
    return (
     <div className="bg3">
   <MDBContainer>
   <MDBAnimation reveal type="flipInX">
   <h2 className="h1-responsive text-center text-black"> <MDBIcon icon="smile-beam" /> Les Bienfaits De Notre Huile</h2>
   </MDBAnimation>
   <p className="text-center w-responsive mx-auto text-grey">
   Pour vous donner des idées, voici une sélection de astuces qui
    feront passer un excellent moment à vos convives. 
    Vous trouverez le plat parfait ave notre huile d'olive.
          </p>
          <MDBAnimation reveal type="bounceInUp">  <MDBRow>
  
      { this.state.astuces.map(astuce =>
        <MDBCol key={astuce.id}  size="4" className='md-0 mb-4'>
         
          <MDBCard >
            <MDBCardImage hover zoom className="img-fluid" src={ 'http://127.0.0.1:8000/uploads/images/'+ astuce.image } />
            <MDBCardBody >
              <MDBCardTitle className="text-uppercase font-weight-bold color-light-green">
              <MDBIcon icon="simle-beam" /> {astuce.nom}</MDBCardTitle>
              <MDBCardText> {astuce.description} </MDBCardText>
            
            </MDBCardBody>
            <Link  to={'astuce/' + astuce.id}  className="btn  btn-light-green">
            
            Savoir Plus </Link>
            <br></br>
          </MDBCard>
        
        </MDBCol>
         )} 
      </MDBRow>
      </MDBAnimation> 

      </MDBContainer>
      </div>
    )
  }
}

