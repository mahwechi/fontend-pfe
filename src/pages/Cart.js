import React from "react";
import CartItem from "../components/CartItem";
//import ProductsApi from "../api/products";
import {connect} from "react-redux";
import {clearCart} from "../store/actions/actions";
import {
   
    MDBContainer,
    MDBRow,
    MDBCol,MDBBtn
  } from "mdbreact";
  
class Cart extends React.Component{

    placeOrder = () => {
        // send the request to the server
        // clear cart
        window.location = "/login";
        this.props.clearCart();
        alert('We recieved your order, and we are working on it.');
       
    };

    render(){
        return (
            <MDBContainer>
                <h3 className="font-weight-bold h1-responsive text-center " style={{padding:"40px"}}>Mon Panier</h3>

                <MDBRow center>
               
                    {this.props.cartItems.map((item, index) => 
                        <div className={'col-12'} key={index}>
                            <CartItem item={item} index={index} />
                        </div>
                    )}
            

                <br />
                
                      <MDBCol size="12"> <h3 className="font-weight-bold h3-responsive text-center ">
                    Total: {this.props.total} Dinar
                </h3></MDBCol>
                <MDBCol size="12" >
                <div className="d-flex justify-content-center">
                <MDBBtn rounded outline color="light-blue"> Retour Au Produit </MDBBtn>
                <MDBBtn rounded className="btn btn-light-blue  " onClick={this.placeOrder}>Finaliser ma Commander</MDBBtn> <br></br>
                </div></MDBCol>   </MDBRow><br></br>
           </MDBContainer>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        cartItems: state.cart,
        total: state.cart.reduce((total, item) => total + item.quantity * item.product.price, 0),
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        clearCart: () => dispatch(clearCart()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
