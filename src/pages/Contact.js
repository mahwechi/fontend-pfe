import React, { Component } from "react";
import './index.css';
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBBtn,
  MDBInput,
  MDBContainer
} from "mdbreact" ;
import { MDBAnimation } from "mdbreact";
import axios from "axios";

export default class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      sujet: "",
      message: "",
      
    };
  }
 
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  submitHandler = e => {
    e.preventDefault();
    console.log(this.state);
    axios
      .post("http://127.0.0.1:8000/api/contacts.json", this.state)
      .then(response => {
        
        console.log(response);
        
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
   
    const { email, sujet, message } = this.state;
  
    return (
      
      <>
       <div className="bg2">
       <MDBContainer>
        <section ><MDBAnimation type="rollIn">
          <h2 className="h1-responsive font-weight-bold text-center my-5">
            Contactez-nous 
          </h2></MDBAnimation>
          <p className="text-center w-responsive mx-auto pb-5">
          Entrez vos information et votre message dans le formulaire
          ci-dessous nous vous répondrons dans les plus brefs délais.
          </p>
          <MDBRow>   
            <MDBCol lg="5" className="lg-0 mb-4">
              <MDBCard>
                <MDBCardBody>
                  <div className=" blue accent-1">
                    <h3 className="mt-2 white">
                      <MDBIcon icon="envelope" /> Ecrivez Ici:
                    </h3>
                  </div>
                  <p className="dark-grey-text">
                    *Toutes les champs sont obligatoires 
                  </p>
                  <form onSubmit={this.submitHandler}>
                  <div className="md-form">
                    <MDBInput
                      icon="envelope"
                      label="Votre adresse email"
                      iconClass="grey-text"
                      type="text"
                      
                      name="email"
                     value={email}
                    onChange={this.changeHandler}
                    required
                    />
                  </div>
                  <div className="md-form">
                    <MDBInput
                      icon="tag"
                      label="Sujet"
                      iconClass="grey-text"
                      type="text"
                      name="sujet"
                      value={sujet}
                      onChange={this.changeHandler} required
                    />
                  </div>
                  <div className="md-form">
                    <MDBInput
                      icon="pencil-alt"
                      label="Votre message"
                      iconClass="grey-text"
                      type="textarea"
                      name="message"
                      value={message}
                     onChange={this.changeHandler} required
                    /> 
                  </div>
                  <div className="text-center">
                    <MDBBtn color="light-blue"   onClick={() => {
        alert.show('Oh look, an alert!')
      }}  type="submit">Submit</MDBBtn>
                  </div>
                  </form>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
            <MDBCol lg="7">
              <div
                id="map-container"
                className="rounded z-depth-1-half map-container"
                style={{ height: "400px" }} >
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3245.549549846506!2d10.869646714977865!3d35.564825480221614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13021a39dda67061%3A0x7ef56e4ea4bbc34a!2sHuilerie%20LOUED!5e0!3m2!1sen!2stn!4v1583259151124!5m2!1sen!2stn"
                  title="This is a unique title"
                  width="100%"
                  height="100%"
                  frameBorder="0"
                  style={{ border: 0 }}>
                </iframe>
              </div>
              <br />
              <MDBRow className="text-center">
                <MDBCol md="4">
                  <MDBBtn tag="a" floating color="light-green" className="accent-1">
                    <MDBIcon icon="map-marker-alt" />
                  </MDBBtn>
                  <p>Les Berges du Lac 1</p>
                  <p className="mb-md-0">1053 Tunis</p>
                </MDBCol>
                <MDBCol md="4">
                  <MDBBtn tag="a" floating color="light-green" className="accent-1">
                    <MDBIcon icon="phone" />
                  </MDBBtn>
                  <p>+ 216 73 400 448</p>
                  <p className="mb-md-0">Mon - Fri, 8:00-17:00</p>
                </MDBCol>
                <MDBCol md="4">
                  <MDBBtn tag="a" floating color="light-green" className="accent-1">
                    <MDBIcon icon="envelope" />
                  </MDBBtn>
                  <p>lexi.huile@gnet.com</p>
                  <p className="mb-md-0">sale@gmail.com</p>
                </MDBCol>
              </MDBRow>
            </MDBCol>
          </MDBRow>
        </section>
        </MDBContainer> </div><br></br>
      </>
    );
  }
}
