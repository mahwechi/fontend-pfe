import React, { Component } from "react";
import {
  MDBMask,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBView,
  MDBContainer
} from "mdbreact";
import {
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBCarousel,
  
  MDBCarouselInner,
  MDBCarouselItem
} from "mdbreact";
import banner from "./banner";
import { BrowserRouter as Router } from "react-router-dom";
import { MDBCardImage, MDBBadge } from "mdbreact";
import "./HomePage.css";
import { MDBAnimation } from "mdbreact";

import img from "../assets/photo/bannier2.jpg";
import img3 from "../assets/photo/bannier3.jpg";
import img4 from "../assets/photo/123.jpg";
import img5 from "../assets/photo/124.jpg";
import img6 from "../assets/photo/125.jpg";
import img8 from "../assets/photo/44.jpg";
import img9 from "../assets/photo/45.jpg";
import img10 from "../assets/photo/46.jpg";
class MinimalisticIntro extends Component {
  render() {
    return (
      <>
        <MDBCarousel
          activeItem={1}
          length={3}
          showControls={true}
          showIndicators={true}
          className="z-depth-1"
        >
          <MDBCarouselInner>
            <MDBCarouselItem itemId="1">
              <MDBView>
                <img className="d-block w-100" src={img} alt="First slide" />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="2">
              <MDBView>
                <img className="d-block w-100" src={img} alt="Second slide" />
              </MDBView>
            </MDBCarouselItem>
            <MDBCarouselItem itemId="3">
              <MDBView>
                <img className="d-block w-100" src={img3} alt="Third slide" />
              </MDBView>
            </MDBCarouselItem>
          </MDBCarouselInner>
        </MDBCarousel>
        <main>
        <MDBContainer >
        
            <MDBCardBody className="text-center">
              <h2 className="h1-responsive font-weight-bold text-center">
                Nos Catégories
              </h2>
              <p className="text-center w-responsive mx-auto">
                Nos principales catégories sont comme suit.. Visitez notre
                magasin . Plusieurs produits en exclusivité Vente en détails .
              </p>
              <MDBAnimation reveal type="fadeInLeft">
                <MDBRow>
                  <MDBCol lg="3" md="12" className="mb-lg-0 mb-4">
                    <MDBView hover className="rounded  mb-4" waves>
                      <img className="img-fluid" src={img10} alt="" />
                      <MDBMask overlay="white-slight" />
                    </MDBView>
                    <a href="#!" className="text-warning">
                      <h6 className="font-weight-bold mb-3">
                        <MDBIcon icon="fire" className="pr-2" />
                        Huile D'olive
                      </h6>
                    </a>
                    <h4 className="font-weight-bold mb-3">
                      <strong>Extra vierge</strong>
                    </h4>
                    <p>
                      by{" "}
                      <b className="font-weight-bold">
                        Rivière D'or
                      </b>
                    </p>
            
                    <MDBBtn
                      className="btn btn-light-green btn-rounded"
                      rounded
                      size="md"
                    >
                      Savoir plus
                    </MDBBtn>
                  </MDBCol>
                  <MDBCol lg="3" md="12" className="mb-lg-0 mb-4">
                    <MDBView hover className="rounded -2 mb-4" waves>
                      <img className="img-fluid" src={img8} alt="" />
                      <MDBMask overlay="white-slight" />
                    </MDBView>
                    <a href="#!" className="text-warning">
                      <h6 className="font-weight-bold mb-3">
                        <MDBIcon icon="fire" className="pr-2" />
                        Huile D'olive
                      </h6>
                    </a>
                    <h4 className="font-weight-bold mb-3">
                      <strong>Collection premimum</strong>
                    </h4>
                    <p>
                      by{" "}
                      <b className="font-weight-bold">
                        Rivière D'or
                      </b>
                    </p>
                   
                    <MDBBtn className="btn btn-light-green " rounded size="md">
                      Savoir plus
                    </MDBBtn>
                  </MDBCol>
                  <MDBCol lg="3" md="12" className="mb-lg-0 mb-4">
                    <MDBView hover className="rounded  mb-4" waves>
                      <img className="img-fluid" src={img9} alt="" />
                      <MDBMask overlay="white-slight" />
                    </MDBView>
                    <a href="#!" className="text-warning">
                      <h6 className="font-weight-bold mb-3">
                        <MDBIcon icon="fire" className="pr-2" />
                        Huile D'olive
                      </h6>
                    </a>
                    <h4 className="font-weight-bold mb-3">
                      <strong>Collection Bio</strong>
                    </h4>
                    <p>
                      by{" "}
                      <b className="font-weight-bold">
                        Rivière D'or
                      </b>
                    </p>
               
                    <MDBBtn className="btn btn-light-green " rounded size="md">
                      Savoir plus
                    </MDBBtn>
                  </MDBCol>
                  <MDBCol lg="3" md="12" className="mb-lg-0 mb-4">
                    <MDBView hover className="rounded  mb-4" waves>
                      <img className="img-fluid" src={img10} alt="" />
                      <MDBMask overlay="white-slight" />
                    </MDBView>
                    <a href="#!" className="text-warning">
                      <h6 className="font-weight-bold mb-3">
                        <MDBIcon icon="fire" className="pr-2" />
                        Huile D'olive
                      </h6>
                    </a>
                    <h4 className="font-weight-bold mb-3">
                      <strong>Les Saveurs</strong>
                    </h4>
                    <p>
                      by{" "}
                      <b className="font-weight-bold ">Rivière D'or</b>
                    </p>
                  
                    <MDBBtn className="btn btn-light-green" rounded size="md">
                      Savoir plus
                    </MDBBtn>
                  </MDBCol>
                </MDBRow>
              </MDBAnimation>
            </MDBCardBody>
          
          </MDBContainer>
         
          <banner />
          <MDBContainer >
            <section className="text-centery ">
              <h2 className="h1-responsive font-weight-bold text-center ">
              Nos Meilleurs Vente
              </h2>
              <p className="text-center w-responsive mx-auto">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit,
                error amet numquam iure provident voluptate esse quasi,
                veritatis totam voluptas nostrum quisquam eum porro a pariatur
                veniam.
              </p>
              <MDBAnimation type="flipInX">
                <MDBRow>
                  <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                    <MDBCard className="align-items-center">
                      <MDBCardImage
                        src={img4}
                        top
                        alt="sample photo"
                        overlay="white-slight"
                      />
                      <MDBCardBody className="text-center">
                      
                      </MDBCardBody>
                    </MDBCard>
                  </MDBCol>
                  <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                    <MDBCard className="align-items-center">
                      <MDBCardImage
                        src={img4}
                        top
                        alt="sample photo"
                        overlay="white-slight"
                      />
                     
                    </MDBCard>
                  </MDBCol>
                  <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                    <MDBCard className="align-items-center">
                      <MDBCardImage
                        src={img6}
                        top
                        alt="sample photo"
                        overlay="white-slight"
                      />
                      
                    </MDBCard>
                  </MDBCol>
                  <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
                    <MDBCard className="align-items-center">
                      <MDBCardImage
                        src={img5}
                        top
                        alt="sample photo"
                        overlay="white-slight"
                      />
                      
                    </MDBCard>
                  </MDBCol>
                </MDBRow>
              </MDBAnimation>
            </section>
          </MDBContainer>
          <br></br>
        </main>
      </>
    );
  }
}

export default MinimalisticIntro;
