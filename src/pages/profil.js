import React, { Component } from 'react'
import axios from 'axios';
import {  MDBRow, MDBCol, MDBCard, MDBCardBody, MDBIcon, MDBBtn, MDBContainer} from "mdbreact";


 class profil extends Component {
     state = {
         user: [],
        
     }
     componentDidMount(){
         let id = this.props.match.params.product_id;
         axios.get(`http://127.0.0.1:8000/api/login/` + id)
         .then(res => {
            this.setState({
                user: res.data
            })
           
          })  
        };

     
    render() {

        return (
            <>
                    <MDBCard className="my-5 text-center">
            <MDBCardBody>
              <h2 className="h1-responsive font-weight-bold my-5">
             Détails Sur le produit
              </h2>
              <p className="grey-text w-responsive mx-auto mb-5">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit,
                error amet numquam iure provident voluptate esse quasi, veritatis
                totam voluptas nostrum quisquam eum porro a pariatur veniam.
              </p>
              <MDBRow className="text-md-left">
                <MDBCol lg="6" md="12" className="mb-5">
                
                  <MDBCol md="8" lg="6" className="float-right">
                    <h4 className="font-weight-bold mb-3"> {this.state.user.username}</h4>
                    <h6 className="font-weight-bold blue-text mb-3">
                   Descritpion du produit : 
                    </h6>
                   
                  
                   
                    <MDBBtn tag="a"  color="light-green" >
                      <MDBIcon icon="shopping-cart" /> Ajouter au panier 
                    </MDBBtn>
                   
                  </MDBCol>
                </MDBCol>
                </MDBRow>
                </MDBCardBody>
                </MDBCard>
        ) 
            </>
        )
    }
}
export default profil;

