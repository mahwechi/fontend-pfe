import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';
import "./HomePage.css";
const FormPage = () => {
return (
<MDBContainer className="background" fluid>
  <MDBRow>
    <MDBCol md="12">
    <form>
        <p className="h5 text-center mb-4">Sign up</p>
        <div className="grey-text">
          <MDBInput label="Your name" icon="user" group type="text" validate error="wrong"
            success="right" />
          <MDBInput label="Your email" icon="envelope" group type="email" validate error="wrong"
            success="right" />
          <MDBInput label="Confirm your email" icon="exclamation-triangle" group type="text" validate
            error="wrong" success="right" />
          <MDBInput label="Your password" icon="lock" group type="password" validate />
        </div>
        <div className="text-center">
          <MDBBtn color="success">Register</MDBBtn>
        </div>
        <div className="text-center">
        You have an account ? <a href="/login"> Click here to login </a>
         
        </div>
      </form>
    
    </MDBCol>
  </MDBRow>
</MDBContainer>
);
};

export default FormPage;