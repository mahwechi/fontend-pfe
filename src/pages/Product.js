import React from "react";
import {getById} from "../api/products";
import {addToCart} from "../store/actions/actions";
import {connect} from "react-redux";

import {
   
    MDBContainer,
    MDBRow,
    MDBBtn
  } from "mdbreact";
  
class Product extends React.Component{

    state={
        loading: true,
        quantity: 0,
        product: {}
    };

    componentDidMount(){
        const id = this.props.match.params.id;

        getById(parseInt(id))
            .then(product => {
                this.setState({
                    product,
                    loading: false
                });
            })
    }

    handleQuantity = (event) => {
        const value = event.target.value;

        if(value < 0)
            return ;

        this.setState({
            quantity: value
        })
    }

    addToCart = (product) => {
        this.props.addToCart(product, this.state.quantity);
    }

    render(){
        if(this.state.loading)
            return 'Loading ..';

        const product = this.state.product;
        const quantity = this.state.quantity;

        return (
            <MDBContainer>
                <MDBRow>
                <h2 className="h1-responsive font-weight-bold mx-auto ">
             Détails Sur le produit
              </h2>
              <p className="grey-text w-responsive mx-auto ">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit,
                error amet numquam iure provident voluptate esse quasi, veritatis
                totam voluptas nostrum quisquam eum porro a pariatur veniam.
              </p>
                    <div className="col-6">
                        <img src={ 'http://127.0.0.1:8000/uploads/images/'+ product.image } alt="" width={'100%'}/>
                    </div>
                    <div className="col-6">
                    <h1 >{product.name}</h1>

                        <p className="font-weight-bold text-primary" >Prix : {product.price} TND</p>

                        <p >{product.description}</p>

                        <div className="col">
                            <h5 className="font-weight-bold light-green-text"> Voulez vous acheter ce produits ? </h5>
                        <label>Préciser la quantité : </label><input 
                         className="form-control" type="number" value={quantity} onChange={this.handleQuantity} />
                        </div>
                        <br></br>
                        <p className="font-weight-bold" > Total de prix en dinar : {quantity * product.price}</p> 
                        <MDBBtn className="btn btn-light-blue" onClick={() => this.addToCart(product)}>
                            Ajouter Au panier
                        </MDBBtn>

                    </div>
                    </MDBRow>
                </MDBContainer>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (productsInfo, quantity) => dispatch(addToCart(productsInfo, quantity)),
    };
}

export default connect(null, mapDispatchToProps)(Product);
