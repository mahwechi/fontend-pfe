import React, { Component } from 'react'
import axios from 'axios';
import {  MDBRow, MDBCol, MDBCard, MDBCardBody, MDBIcon, MDBBtn} from "mdbreact";


 class drecette extends Component {
     state = {
         recette:  {}
        
     }
     componentDidMount(){
        
          const id = this.props.match.params.id;

          axios.get(`http://127.0.0.1:8000/api/recettes/` + id)
              .then(recette => {
                  this.setState({
                      recette,
                    
                  });
              })
        };

     
    render() {
        const recette = this.state.recette;
        return (
            <>
                    <MDBCard className="my-5 px-5 pb-1 text-center">
            <MDBCardBody>
              <h2 className="h1-responsive font-weight-bold my-5">
             Détails Recette
              </h2>
              <p className="grey-text w-responsive mx-auto mb-5">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit,
                error amet numquam iure provident voluptate esse quasi, veritatis
                totam voluptas nostrum quisquam eum porro a pariatur veniam.
              </p>
              <MDBRow className="text-md-left">
                <MDBCol lg="6" md="12" className="mb-5">
                  <MDBCol md="4" lg="6" className="float-left">
                    <img
                      src={ 'http://127.0.0.1:8000/uploads/images/' + recette.image }
                      className="mx-auto mb-md-0 mb-4 rounded z-depth-1 img-fluid"
                      tag="img"
                      alt="Sample avatar"
                    />
                  </MDBCol>
                  <MDBCol md="8" lg="6" className="float-right">
                    <h4 className="font-weight-bold mb-3"> {recette.nom}</h4>
                    <h6 className="font-weight-bold blue-text mb-3">
                   Ingredients : 
                    </h6>
                    <p className="grey-text">
                    {recette.ingredients}
                    </p>
                    <h6 className="font-weight-bold blue-text mb-3">
                   Préparations 
                    </h6>
                    <p className="black-text">
                    {recette.preparations} 
                    </p>
                    <MDBBtn tag="a"  color="light-green" >
                      <MDBIcon icon="shopping-cart" /> Ajouter au panier 
                    </MDBBtn>
                   
                  </MDBCol>
                </MDBCol>
                </MDBRow>
                </MDBCardBody>
                </MDBCard>
        ) 
            </>
        )
    }
}
export default drecette;

