import React from "react";
import { connect } from "react-redux";
import {removeFromCart} from "../store/actions/actions";
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';

function ProduitItem(props){
    const {item, index} = props;
    const {produit} = item;

    return (                     
<>
<MDBTable>
      <MDBTableHead color="cloudy-knoxville-gradient" >
        <tr>
          <th>Image</th>
          <th>Nom Du Produit</th>
          <th>Prix</th>
          <th>Quantité </th>
          <th>Total </th>
      
        </tr>
      </MDBTableHead>
      <MDBTableBody>
        <tr>
          <td><img style={{ width:"150px"}} src={ 'http://127.0.0.1:8000/uploads/images/'+ produit.image } className="img-thumbnail" alt="..." />
</td>
          <td>{produit.nom}</td>
          <td> {produit.prix} TND</td>
          <td>{item.quantity}</td>
          <td> <b>{item.quantity * produit.prix}  TND</b></td>
          <td>   <button onClick={() => props.removeFromCart(index)} className="btn btn-danger">
                    <i className="fa fa-trash"></i> Supprimer
                </button> </td>
        </tr>
       
      </MDBTableBody>
    </MDBTable>
      


        </>
    );
}

export default connect(null, {removeFromCart})(ProduitItem);
