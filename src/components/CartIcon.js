import React from "react";
import { Link } from "react-router-dom";
import {connect} from "react-redux";
import "./CartIcon.css";
import {
    MDBIcon, MDBBadge,MDBBtn 
  } from "mdbreact";

function CartIcon(Props){
    return <div id="cart-icon"  >
        <MDBBtn color="light-green" > 
         <Link to="/cart" style={{ color:"white" }} >
         <h6 className="font-weight-bold h6-responsive">Panier  <MDBIcon icon="shopping-cart" style={{ color:"white" }} /> <MDBBadge size="lg" pill color="danger" className="ml-4">  {Props.totalQuantity}</MDBBadge></h6>
       </Link> </MDBBtn>
    </div>

     
}

const mapStateToProps = (state) => {
    return {
        totalQuantity: state.cart.reduce((total, item) => total + parseInt(item.quantity), 0),
    };
}

export default connect(mapStateToProps)(CartIcon);