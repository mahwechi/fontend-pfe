import React from "react";
import {
   
    MDBCard,
    MDBCardBody,
    MDBCardImage
  } from "mdbreact";
  
export default function ProduitItem(props){
    const {produit} = props;
    

    return (
        <>
        <MDBCard zoom style={{ padding:"10px"}}>
            <MDBCardImage hover zoom src={ 'http://127.0.0.1:8000/uploads/images/'+ produit.image } className="card-img-top" alt="..." />
            <MDBCardBody>
                <h5 className="card-title">
                    {produit.nom}
                </h5>
    
                <h6 className="card-text font-weight-bold ">
                    Prix {produit.prix} TND
                </h6>
                <h6 className="card-text font-weight-bold ">
    categorie 
                </h6>
                
                <a href={"/produits/" + produit.id} className="btn btn-primary">Details</a>
            </MDBCardBody>
        </MDBCard>
      
        </>
    );
}
