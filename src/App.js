import React, { Component } from "react";
import CartIcon from "./components/CartIcon";
import {
  MDBNavbar,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBCollapse,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarBrand,
  MDBFooter,
  MDBContainer,
  MDBRow,
  MDBCol
} from "mdbreact";
import logo from "./assets/logo-09.png";
import {Provider} from "react-redux";
import store from "./store/store";

import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";

import "./style.css";

class App extends Component {
  state = {
    modal8: false,
    modal9: false
  };

  toggle = nr => () => {
    let modalNumber = "modal" + nr;
    this.setState({
      [modalNumber]: !this.state[modalNumber]
    });
  };

  state = {
    collapseID: ""
  };

  toggleCollapse = collapseID => () =>
    this.setState(prevState => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : ""
    }));

  closeCollapse = collID => () => {
    const { collapseID } = this.state;
    window.scrollTo(0, 0);
    collapseID === collID && this.setState({ collapseID: "" });
  };

  render() {
    const overlay = (
      <div
        id="sidenav-overlay"
        style={{ backgroundColor: "transparent" }}
        onClick={this.toggleCollapse("mainNavbarCollapse")}
      />
    );

    const { collapseID } = this.state;

    return (
      <>
        <Router>
        
            <MDBNavbar
              color="white-color"
              light
              expand="md"
              fixed="top"
              scrolling
            >
              <MDBNavbarBrand id="logo" href="#">
                <img src={logo} height="120" alt="" />
              </MDBNavbarBrand>

              <MDBNavbarToggler
                onClick={this.toggleCollapse("mainNavbarCollapse")}
              />
              <MDBCollapse id="mainNavbarCollapse" isOpen={collapseID} navbar>
                <MDBNavbarNav>
                  <MDBNavItem>
                    <MDBNavLink
                      exact
                      to="/"
                      onClick={this.closeCollapse("mainNavbarCollapse")}
                    >
                      <strong className="link"> Accueil </strong>
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      onClick={this.closeCollapse("mainNavbarCollapse")}
                      to="/Products"
                    >
                      <strong className="link"> Nos Produits </strong>
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      onClick={this.closeCollapse("mainNavbarCollapse")}
                      to="/recettes"
                    >
                      <strong className="link">Recettes</strong>
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      onClick={this.closeCollapse("mainNavbarCollapse")}
                      to="/bienfaits"
                    >
                      <strong className="link">Bienfaits</strong>
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      onClick={this.closeCollapse("mainNavbarCollapse")}
                      to="/contact"
                    >
                      <strong className="link">Contact</strong>
                    </MDBNavLink>
                  </MDBNavItem>
                
                  {/* <MDBNavItem>
                    <MDBBtn tag="a" color="light-blue">
                      <MDBIcon icon="user" />
                    </MDBBtn>
                  </MDBNavItem> */}
                  <CartIcon />
                </MDBNavbarNav>
              </MDBCollapse>
          
            </MDBNavbar>
            {collapseID && overlay}
            <main style={{ marginTop: "8rem" }}>
              <Routes />
              </main>
       
        </Router>
        <MDBFooter color="light-green" className="font-small pt-0">
          <MDBContainer>
            <hr className="rgba-white-light" style={{ margin: "0 15%" }} />
            <MDBRow className="d-flex text-center justify-content-center mb-md-0 mb-4">
              <MDBCol md="8" sm="12" className="mt-5">
                <p style={{ lineHeight: "1.7rem" }}>
                  Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                  accusantium doloremque laudantium consequuntur.
                </p>
              </MDBCol>
            </MDBRow>
            <hr
              className="clearfix d-md-none rgba-white-light"
              style={{ margin: "10% 15% 5%" }}
            />
          </MDBContainer>
          <div className="footer-copyright text-center py-3">
            <MDBContainer fluid>
              &copy; {new Date().getFullYear()} Copyright:
              <a href="https://www.MDBootstrap.com"> MDBootstrap.com </a>
            </MDBContainer>
          </div>
        </MDBFooter>{" "}
      </>
    );
  }
}
function  AppWithStore(){
  return <Provider store={store}>
    <App />
  </Provider>
}

export default  AppWithStore;
